<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Catalog
Route::get('/catalog', 'ItemController@index');
Route::get('/catalog/{id}', 'ItemController@filter');
Route::get('/catalog/sort/{sort}', 'ItemController@sort');
Route::post('/search', 'ItemController@search');

Route::middleware("admin")->group(function(){

	// Add Item
	Route::get('/additem', 'ItemController@create');
	// To save
	Route::post('/additem', 'ItemController@store');
	// To delete
	Route::delete('/deleteitem/{id}', 'ItemController@destroy');
	// To go to edit form
	Route::get('/edititem/{id}', 'ItemController@edit');
	// To save the edited item
	Route::patch('/edititem/{id}', 'ItemController@update');	

	Route::delete('/removeitem/{id}', 'ItemController@removeItem');

	Route::get('/allorders', 'OrderController@allOrders');

	// Users
	Route::get('/allusers', 'UserController@index');

	Route::get('/changerole/{id}', 'UserController@changeRole');

	Route::delete('/deleteuser/{id}', 'UserController@destroy');
});

Route::middleware("user")->group(function(){

	// Cart CRUD
	Route::post('/addtocart/{id}', 'ItemController@addToCart');
	Route::get('/showcart', 'ItemController@showCart');


	Route::delete('/emptycart', 'ItemController@emptyCart');

	Route::get('/checkout', 'OrderController@checkOut');

	Route::get('/showorders', 'OrderController@showOrders');
});

Route::middleware('auth')->group(function(){
	Route::get('/cancelorder/{id}', 'OrderController@cancelOrderByAdmin');
});

